<?php

namespace CckPay\Services;

use GuzzleHttp\Client;

/**
 * 支付数据服务
 */
class PayService 
{

    /**
     * @var object HTTP请求类 
     */
    private $http;
    
    /**
     * @var array 存放客户端认证信息
     */
    private $auth;
    
    /**
     * @var array 存放PAY配置
     */
    private $config;

    /**
     * 初始化
     */
    public function __construct() 
    {
        $this->config = config('pay');
        
        $this->http = new Client([
            'base_uri' => $this->config['server_uri'],
            'headers' => [
                'User-Agent' => 'CaiCaiKan PAY V1.0',
            ],
        ]);
        
        $this->auth = [
            'client_id' => $this->config['client_id'],
            'client_secret' => $this->config['client_secret'],
        ];
    }
    
    /**
     * 创建客户端
     * 
     * @param string $name 客户端名称(项目名)
     * @param string $domains 允许的域名，多个之间请用逗号分隔开
     * @return array
     */
    public function createClient($name, $domains)
    {
        $data = ['name' => $name, 'domains' => $domains];
        $response = $this->http->request('POST', $this->config['server_apis']['client_create'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * API: 下单并返回二维码图片 (PC)
     * 
     * @param array $data 参数项
     * @example $data [
     *     'order_id' => '1234567890', // string【非必须】订单号，最大长度为36，不传会自动生成
     *     'total_fee' => '6.66',      // string | float 【必须】 订单总金额，单位是元，精确到小数点后两位
     *     'payment_method' => 1,      // int 【必须】 支付方式 （1：支付宝，2：微信）
     *     'payment_mark' => '描述',   // string 【非必须】 支付描述信息
     *     'goods_id' => 123,          // int 【非必须】 购买商品的唯一ID标识
     *     'goods_num' => 2,           // int 【非必须】 购买商品的数量
     *     'goods_price' => '3.33'，   // string | float 【非必须】 购买商品的单价
     *     'user_id' => 123456,        // int 【非必须】 购买者的用户ID
     * ]
     */
    public function genQrcodeOrder(array $data)
    {
        $data['payment_channel'] = 'PC';
        $data += $this->auth;
        $response = $this->http->request('POST', $this->config['server_apis']['gen_order_qrcode'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * API: 下单并返回移动网页H5支付链接
     * 
     * @param array $data 参数项
     * @example $data [
     *     'order_id' => '1234567890', // string【非必须】订单号，最大长度为36，不传会自动生成
     *     'total_fee' => '6.66',      // string | float 【必须】 订单总金额，单位是元，精确到小数点后两位
     *     'payment_method' => 1,      // int 【必须】 支付方式 （1：支付宝，2：微信）
     *     'payment_mark' => '描述',   // string 【非必须】 支付描述信息
     *     'goods_id' => 123,          // int 【非必须】 购买商品的唯一ID标识
     *     'goods_num' => 2,           // int 【非必须】 购买商品的数量
     *     'goods_price' => '3.33'，   // string | float 【非必须】 购买商品的单价
     *     'user_id' => 123456,        // int 【非必须】 购买者的用户ID
     *     'return_url' => 'http://x'  // string 【非必须】 支付成功后同步返回的页面URL地址
     * ]
     */
    public function genWapOrder(array $data)
    {
        $data['payment_channel'] = 'WAP';
        $data += $this->auth;
        $response = $this->http->request('POST', $this->config['server_apis']['gen_order_wap'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * API: 下单并返回APP支付需要参数项
     * 
     * @param array $data 参数项
     * @example $data [
     *     'order_id' => '1234567890', // string【非必须】订单号，最大长度为36，不传会自动生成
     *     'total_fee' => '6.66',      // string | float 【必须】 订单总金额，单位是元，精确到小数点后两位
     *     'payment_method' => 1,      // int 【必须】 支付方式 （1：支付宝，2：微信）
     *     'payment_mark' => '描述',   // string 【非必须】 支付描述信息
     *     'goods_id' => 123,          // int 【非必须】 购买商品的唯一ID标识
     *     'goods_num' => 2,           // int 【非必须】 购买商品的数量
     *     'goods_price' => '3.33'，   // string | float 【非必须】 购买商品的单价
     *     'user_id' => 123456,        // int 【非必须】 购买者的用户ID
     * ]
     */
    public function genAppOrder(array $data)
    {
        $data['payment_channel'] = 'APP';
        $data += $this->auth;
        $response = $this->http->request('POST', $this->config['server_apis']['gen_order_app'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * API: 下单并返回JSAPI支付需要参数项
     * 
     * @param array $data 参数项
     * @example $data [
     *     'open_id' => 'abcde123456'，// string【必须】 第三方OPENID
     *     'order_id' => '1234567890', // string【非必须】订单号，最大长度为36，不传会自动生成
     *     'total_fee' => '6.66',      // string | float 【必须】 订单总金额，单位是元，精确到小数点后两位
     *     'payment_method' => 1,      // int 【必须】 支付方式 （1：支付宝，2：微信）
     *     'payment_mark' => '描述',   // string 【非必须】 支付描述信息
     *     'goods_id' => 123,          // int 【非必须】 购买商品的唯一ID标识
     *     'goods_num' => 2,           // int 【非必须】 购买商品的数量
     *     'goods_price' => '3.33'，   // string | float 【非必须】 购买商品的单价
     *     'user_id' => 123456,        // int 【非必须】 购买者的用户ID
     * ]
     */
    public function genJsapiOrder(array $data)
    {
        $data['payment_channel'] = 'JS';
        $data += $this->auth;
        $response = $this->http->request('POST', $this->config['server_apis']['gen_order_jsapi'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }

    /**
     * 查询订单信息
     * 
     * @param string $orderId 订单号
     */
    public function queryOrder($orderId)
    {
        $data = $this->auth;
        $data['order_id'] = $orderId;
        $response = $this->http->request('POST', $this->config['server_apis']['query_order'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * 转账
     * 
     * @param array $data 参数项
     * @example $data [
     *     'order_id' => '1234567890', // string【非必须】订单号，最大长度为32，不传会自动生成
     *     'transfer_method' => 1,      // int 【必须】 转账方式 （1：支付宝，2：微信）
     *     'transfer_mark' => '描述',   // string 【非必须】 转账描述信息
     *     'transfer_amount' => '金额', // string | float 【必须】 转账金额，单位是元，小数点后面两位
     *     'open_id' => '第三方唯一ID', // string 【必须】 第三方用户ID （微信：openid, 支付宝:userid）
     * ]
     */
    public function transfer(array $data)
    {
        $data += $this->auth;
        $response = $this->http->request('POST', $this->config['server_apis']['transfer_order'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
        
    /**
     * 解析响应的结果
     * 
     * @param object $response
     * @return array
     */
    protected function parseResponse($response)
    {
        $response = json_decode((string) $response->getBody(), true);
        
        return $response;
    }
    
}
