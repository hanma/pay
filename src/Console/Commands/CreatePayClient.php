<?php

namespace CckPay\Console\Commands;

use Illuminate\Console\Command;
use CckPay\Services\PayService;

/**
 * 创建支付客户端
 */
class CreatePayClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pay:create {name?} {domains?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建支付客户端';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name') ?: $this->ask('输入需要使用PAY服务的客户端（项目）名称');
        $domains = $this->argument('domains') ?: $this->ask('输入支付回调允许的域名，多个请用逗号分隔开');

        $service = new PayService();
        $response = $service->createClient($name, $domains);
        if (isset($response['client_id'])) {
            $this->info('客户端创建成功');
            $this->table(['name', 'client_id', 'client_secret', 'client_domains'], [ [
                $response['client_name'], $response['client_id'], $response['client_secret'], $response['client_domains']
            ] ]);
        } else {
            $this->info('客户端创建失败');
        }
    }
}
