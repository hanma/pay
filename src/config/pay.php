<?php

/**
 * CCK PAY配置文件
 */
return [
    
    /* PAY客户端认证密钥 */
    'client_id' => '',
    'client_secret' => '',
    
    /* PAY服务端API地址 */
    'server_uri' => 'http://caicaikan.loc',
    'server_apis' => [
        'client_create' => '/api/pay/client/create', // 申请创建PAY客户端
        'gen_order_qrcode' => '/api/pay/order/qrcode', // 下单并生成订单二维码 (PC)
        'gen_order_wap' => '/api/pay/order/wap', // 下单并返回移动网页H5支付的链接 (H5)
        'gen_order_app' => '/api/pay/order/app', // 下单并返回APP支付需要的参数项 (APP)
        'gen_order_jsapi' => '/api/pay/order/jsapi', // 下单并生成JSAPI支付需要的参数 (JSAPI)
        'query_order' => '/api/pay/order/query', // 查询订单信息
        'transfer_order' => '/api/pay/order/transfer', // 转账支付
    ],
    
];

