<?php

namespace CckPay\Providers;

use Illuminate\Support\ServiceProvider;
use CckPay\Console\Commands\CreatePayClient;

/**
 * 支付的服务提供者
 */
class PayServiceProvider extends ServiceProvider
{
    /**
     * 注册
     */
    public function register()
    {
        $this->publishes([__DIR__ . '/../config/pay.php' => config_path('pay.php')]);
        
        $this->commands([
            CreatePayClient::class, // 创建客户端
        ]);
    }

    /**
     * 启动
     */
    public function boot()
    {
        
    }
}
